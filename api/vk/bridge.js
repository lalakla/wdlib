wdlib.module("wdlib/api/vk/bridge.js", [
	"wdlib/api/vk/base.js",
	"wdlib/net/http.js",
	"wdlib/utils/date.js"
],
function(exports) {
"use strict";

var _request_id = 1;

// ============================================================================
// wdlib.api.vk.Bridge class

class Bridge extends wdlib.api.vk.Base {

	constructor(args)
	{
		super(args);
		
		console.log("VK.BRIDGE : ", args);

		this.access_token = args.access_token || "";
	}

	init(callback, on_error)
	{
		if(this._init_state != wdlib.api.INIT_STATE_START) {
			// double call of api.init
			var error = new wdlib.model.Error({error: "API INIT ERROR : double call", error_code: wdlib.model.error.ERROR_API});
			error.file = "wdlib.api.vk.bridge.js";

			if(this.on_error_callback) {
				this.on_error_callback.call(null, error);
			}
			return;
		}
		
		super.init(callback, on_error);

		var self = this;
		this._init_state = wdlib.api.INIT_STATE_CALLED;

		wdlib.load([{src: "vk.bridge", type: "js", crossOrigin: null}], function() {

			console.log("VK.BRIDGE loaded");

			if(self._init_state != wdlib.api.INIT_STATE_CALLED) {
				// double call of api.init
				var error = new wdlib.model.Error({error: "API INIT ERROR : double call of init.load_sdk", error_code: wdlib.model.error.ERROR_API});
				error.file = "wdlib.api.vk.bridge.js";

				if(self.on_error_callback) {
					self.on_error_callback.call(null, error);
				}
				return;
			}
			self._init_state = wdlib.api.INIT_STATE_SDK_LOADED;

			try {
				vkBridge.send('VKWebAppInit')
					.then(function() {
						// API INIT OK
						console.log("VK.BRIDGE API: init ok");
						self._init_state = wdlib.api.INIT_STATE_INIT_PASSED;
		
						self.getProfile(self.viewer_id, function(data) {
							if(self._init_state != wdlib.api.INIT_STATE_INIT_PASSED) {
								// double call of api.init
								var error = new wdlib.model.Error({error: "API INIT ERROR : double call of init.getProfile", error_code: wdlib.model.error.ERROR_API});
								error.file = "wdlib.api.vk.bridge.js";

								if(self.on_error_callback) {
									self.on_error_callback.call(null, error);
								}
								return;
							}
							self._init_state = wdlib.api.INIT_STATE_COMPLETE;
							callback.call(null, data, self.auth_key);
						});
					})
					.catch(function(e) {
						console.error("wdlib.api.vk.bridge.js : VKWebAppInit : error recived : ", e);
	
						var error = new wdlib.model.Error({error: e.error_data.error_reason, error_code: wdlib.model.error.ERROR_API, stack: e.stack, file: e.fileName || "wdlib.api.vk.bridge.js"});
						error.error += "; method=VKWebAppInit";
						if(self.on_error_callback) {
							self.on_error_callback.call(null, error);
						}	
					});

			}
			catch(e) {
				console.error("VK API INIT ERROR CATCH : ", e);
				
				var error = new wdlib.model.Error({error: e.message, error_code: wdlib.model.error.ERROR_API, stack: e.stack, file: e.fileName || "wdlib.api.vk.bridge.js"});
				error.error += "; method=INIT";
				if(self.on_error_callback) {
					self.on_error_callback.call(null, error);
				}
			}

		});
	}

	_call(method, params, callback)
	{
		// console.log("wdlib.api.vk.bridge.js : _call : ", method, params);

		var self = this;

		// api version
		params.v = params.v || wdlib.api.vk.API_VERSION;
		// access_token
		params.access_token = this.access_token;

		try {
			var req_id = this.viewer_id + "_" + String(_request_id);
			++_request_id;
			
			vkBridge.send("VKWebAppCallAPIMethod", {
				"method" : method,
				"request_id" : req_id,
				params : params
			}).then(function(data) {
				// console.log("wdlib.api.vk.bridge.js : _call : " + method + " : ", data);
				callback.call(undefined, data);
			}).catch(function(e) {
				console.error("wdlib.api.vk.bridge.js : _call : " + method + " : error recived : ", e);
	
				var error = new wdlib.model.Error({error: e.error_data.error_reason, error_code: wdlib.model.error.ERROR_API, stack: e.stack, file: e.fileName || "wdlib.api.vk.bridge.js"});
				error.error += "; method=" + method;
				error.error += "; params=" + JSON.stringify(params);
				if(self.on_error_callback) {
					self.on_error_callback.call(null, error);
				}	
			});
		}
		catch(e) {
			console.error("wdlib.api.vk.bridge.js : _call : error catch : ", e);

			var error = new wdlib.model.Error({error: e.message, error_code: wdlib.model.error.ERROR_API, stack: e.stack, file: e.fileName || "wdlib.api.vk.bridge.js"});
			error.error += "; method=" + method;
			error.error += "; params=" + JSON.stringify(params);
			if(this.on_error_callback) {
				this.on_error_callback.call(null, error);
			}
		}
	}
	_ui_error(e, method)
	{
		var error = new wdlib.model.Error({error: e.message, error_code: wdlib.model.error.ERROR_API, stack: e.stack, file: e.fileName || "wdlib.api.vk.bridge.js"});
		error.error += "; method=" + method;
		if(this.on_error_callback) {
			this.on_error_callback.call(null, error);
		}
	}

	/**
	 * @param int width
	 * @param int height
	 */
	setAppSize(width, height)
	{
		try {
			vkBridge.send("VKWebAppResizeWindow", {
				"width" : width,
				"height" : height
			});
		}
		catch(e) {
			this._ui_error(e, "setAppSize");
		}
	}

	/**
	 * @param Function callback
	 */
	favouritesDialog(callback)
	{
		try {
			vkBridge.send("VKWebAppAddToFavorites")
				.then(function(data) {
					console.log("wdlib.api.vk.Bridge::favouritesDialog result: ", data);

					var r = data.result ? 1 : 0;
					callback.call(undefined, r);
				}).catch(function(e) {
					console.error("wdlib.api.vk.Bridge::favouritesDialog error: ", e);
				});
		}
		catch(e) {
			this._ui_error(e, "favouritesDialog");
		}
	}

	/**
	 * @param String mess
	 * @param Array uids
	 * @param Function callback
	 * @param Object extra
	 */
	messageDialog(mess, uids, callback, extra)
	{
		// call for ONE user per time !!!
		var user_id = wdlib.utils.intval(uids[0]);

		try {
			vkBridge.send("VKWebAppShowRequestBox", {
				"uid" : user_id,
				"message" : mess,
				"requestKey" : (extra && extra.extra) ? extra.extra : ""
			}).then(function(data) {
				callback.call(undefined, data.success ? [user_id] : undefined);
			}).catch(function(e) {
				callback.call(undefined, undefined);
			});
		}
		catch(e) {
			this._ui_error(e, "messageDialog");
		}
	}
	/**
	 * @param String mess
	 * @param Array uids
	 * @param Function callback
	 * @param Object extra
	 */
	inviteDialog(mess, uids, callback, extra)
	{
		if(uids.length == 1) {
			this.messageDialog(mess, uids, callback, extra);
		}
		else {
			try {
				vkBridge.send("VKWebAppShowInviteBox");
			}
			catch(e) {
				this._ui_error(e, "inviteDialog");
			}

			// call immediatly, cause VK doesn't notify about invites sended ...
			callback.call(undefined, undefined);
		}
	}

	billingDialog(gold, amount, callback, name, desc, pic, extra)
	{
		try {
			vkBridge.send("VKWebAppShowOrderBox", {
				"type" : "item",
				"item" : (amount) ? String(amount) : extra
			}).then(function(data) {
				console.log("wdlib.api.vk.Bridge::billingDialog result: ", data);
				callback.call(undefined, amount, data.success ? wdlib.api.BILLING_TRUE : wdlib.api.BILLING_FALSE);
			}).catch(function(e) {
				// log to server
				wdlib.net.Http.report("/debug/trace", {e: e});

				callback.call(undefined, amount, wdlib.api.BILLING_FALSE);
			});
		}
		catch(e) {
			this._ui_error(e, "messageDialog");
		}
	}

}

exports.Bridge = Bridge;
// ============================================================================

}, (wdlib.api = wdlib.api || {}).vk = wdlib.api.vk || {});
