wdlib.module("wdlib/net/poll.js", [
	"wdlib/net/http.js",
	"wdlib/utils/worker.js"
],
function(exports) {
"use strict";

var _broadcast = undefined;
var _worker = undefined;
var _wuniq = 0;


// ============================================================================
// wdlib.net.PollSocket class

class PollSocket {

	/**
	 * @param string _url
	 * @param string _lpoll_uid
	 */
	constructor(_url, _lpoll_uid)
	{
		this.url = _url;
		this.lpoll_uid= _lpoll_uid;

		this.data = {};
		this.callback = undefined;
		this.error_callback = undefined;

		this.on = false;
		this._abrt = undefined;
		if(typeof AbortController !== "undefined") {
			this._abrt = new AbortController;
		}
		else if (typeof window.AbortController !== "undefined") {
			this._abrt = new window.AbortController;
		}
		this._stop = false;
	}

	/**
	 * @param Object params
	 * @params Function callback
	 * @params Function error_callback
	 */
	connect(params, callback, error_callback)
	{
		if(this.on) {
			console.log("wdlib.net.PollSocket::connect error: already listening");
			return;
		}

		this._stop = false;

		this.callback = callback;
		this.error_callback = error_callback;

		this.data = {};
		for(var k in params) {
			if(params[k] == "") continue;
			if(typeof params[k] === "object") {
				this.data[k] = JSON.stringify(params[k]);
			}
			else {
				this.data[k] = params[k];
			}
		}
		this.data["lpoll_uid"] = this.lpoll_uid;

		this.send();
	}

	send()
	{
		if(this._stop) {
			console.log("wdlib.net.PollSocket::send error: stopped");
			return;
		}

		this.on = true;

		var self = this;

		var _err = function(err)
		{
			console.error("wdlib.net.PollSocket : HTTP ERROR : ", err);
			self.on = false;
			if(self.error_callback !== undefined) {
				self.error_callback.call(undefined, err);
			}
		}

		try {

			var params = {
				method: "post",
				headers: {
					"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
				},
				body: wdlib.net.Http.encode(this.data)
			}

			if(this._abrt) {
				params.signal = this._abrt.signal;
			}

			fetch(this.url, params)
			.then(function(response) {
				if(!response.ok) {
					throw new Error("error while report : " + self.url);
				}
				return response.text();
			}).then(function(resp) {
				// console.log("wdlib.net.PollSocket : RESPONSE : ", resp);

				self.on = false;

				if(!resp || !resp.length) {
					// just reconnect;
					// console.log("wdlib.net.PollSocket : reconnecting...");
					self.send();
					return;
				}
			
				var data = JSON.parse(resp);
				self.callback.call(null, data);

			}).catch(function(e) {
				_err.call(undefined, e);
			});
		}
		catch(e) {
			_err.call(undefined, e);
		}
	}

	stop()
	{
		if(this._abrt) {
			this._abrt.abort();
		}

		this._stop = true;
	}
}

exports.PollSocket = PollSocket;
// ============================================================================

// ============================================================================
// wdlib.net.Poll class
class Poll {

	constructor()
	{
		this.sock = undefined;
		this.err_timeout = 1000;
	}

	init(url, uid, callback)
	{
		this.url = url;
		this.uid = uid;
		this.callback = callback;

		if(_worker) {
			_init_worker();
			return;
		}
		
		this.sock = new wdlib.net.PollSocket(this.url, this.uid);
		this._sock();
	}

	_sock()
	{
		if(this.sock) {
			this.sock.connect({}, this.onSocketData.bind(this), this.onSocketError.bind(this));
		}
	}

	onSocketData(data)
	{
		this.err_timeout = 1000;
		
		// continue listening
		this._sock();

		// processing event
		this.callback.call(undefined, data);
	}

	onSocketError(err)
	{
		console.error("wdlib.net.Poll sokect error ", err, " try reconnect in ", this.err_timeout, " msec");

		var self = this;

		setTimeout(function() {
			self.err_timeout *= 2;

			// continue listening
			self._sock();

		}, this.err_timeout);
	}

	stop()
	{
		if(this.sock) {
			this.sock.stop();
			this.sock = undefined;
		}
	}
}

exports.Poll = new Poll;
// ============================================================================

// ============================================================================
// init SharedWorker here

//if((typeof SharedWorker !== "undefined") && (typeof BroadcastChannel !== "undefined")) {
//	wdlib.utils.worker.create(wdlib.src("wdlib/net/poll-worker.js"), function(worker) {
//		startWorker(worker);
//	});
//}
//else {
//	console.error("wdlib.net.Poll : ERROR CREATING HTTP WORKER");
//}

function startWorker(worker)
{
	if(!worker) {
		return;
	}
	
	console.log("wdlib.net.Poll : create SharedWorker");
	_worker = worker;

	_broadcast = new BroadcastChannel("wdlib.net.poll-worker.js");

	_worker.port.addEventListener("message", function(e) {
		// console.log("WORKER MESSAGE : ", e);
		try {
			var data = JSON.parse(e.data);

			switch(data.cmd) {
				case "uniq":
					_wuniq = data.uniq;
					_init_worker();
					break;
				case "init":
					_on_init(data);
					break;
			}
		}
		catch(e) {
			console.error("POLL WORKER : data error : ", e.toString());
		}
	});

	_worker.port.start();
	_worker.port.postMessage(JSON.stringify({
		cmd: "uniq"
	}));
}

function _init_worker()
{
	if(_worker && _wuniq && wdlib.net.Poll.uid) {
		console.log("wdlib.net.Poll : init worker : ", _wuniq, wdlib.net.Poll.uid);
		_worker.port.postMessage(JSON.stringify({
			cmd: "init",
			uniq: _wuniq,
			uid: wdlib.net.Poll.uid,
			url: wdlib.net.Poll.url
		}));
	}
}

function _on_init(data)
{
	_broadcast.onmessage = function(mess) {
		// console.log("wdlib.net.Poll : broadcast message : ", mess);

		try {
			var data = JSON.parse(mess.data);
			var uid = data.uid;

			if(uid != wdlib.net.Poll.uid) {
				// this event for some other user 
				return;
			}

			wdlib.net.Poll.onSocketData(data.data);
		}
		catch(e) {
			console.error("POLL WORKER : data error : ", e.toString());
		}
	}
}

// ============================================================================

}, (wdlib.net = wdlib.net || {}));
