wdlib.module("wdlib/net/http.js", [
	"wdlib/model/error.js"
],
function(exports) {
"use strict";

var _queue = [];
var _waiting = false;

// ============================================================================
// wdlib.net.Http interface

exports.redirect = function(url)
{
	window.location = url;
}

/**
 * @param String uri
 * @param Object data
 * @param Function callback
 * @param Object udata [optional]
 * @param Object context [optional]
 *
 * sends through queue
 */
exports.send = function(uri, data, callback, udata, context)
{
	_queue.push({
		uri: uri,
		data: data,
		callback: callback,
		udata: udata,
		context: context
	});
	try_send();
}

/**
 * @param String uri
 * @param Object data
 * @param Function callback
 * @param Object udata [optional]
 * @param Object context [optional]
 *
 * sends immediatly
 */
exports.report = function(uri, data, callback, udata, context)
{
	var req = {
		uri: uri,
		data: data,
		callback: callback,
		udata: udata,
		context: context
	};
	_send(req);
}

exports.encode = function(data, sort = false, info = null)
{
	var str = "";
	if(!(typeof data === "object")) {
		return data;
	}

	if((info !== null) && !(typeof info === "object")) {
		info = null;
	}

	var list = [];

	for(var k in data) {
		let _k = encodeURIComponent(k);
		let _v = encodeURIComponent((typeof data[k] === "object") ? JSON.stringify(data[k]) : data[k])

		if(sort || info !== null) {
			list.push({k:_k, v:_v});
		}
		else {
			str += (str.length ? "&" : "") + _k + "=" + _v; 
		}
	}

	if(sort) {
		list.sort(_sort);
	}

	if(info) {
		info.list = list;
	}

	if(!str.length && list.length) {
		str = list.reduce(function(str, e) {
			return (str.length ? "&" : "") + e.k + "=" + e.v;
		});
	}

	return str;
}

// ============================================================================

// ============================================================================
// private methods

function try_send()
{
	if(_waiting || !_queue.length) {
		return;
	}

	_waiting = true;
	var req = _queue.shift();

	var _err = function(err, req)
	{
		console.error("wdlib.net.Http : HTTP SEND FAIL : ", req, err);
		_waiting = false;
		try_send();
	}

	var _ok = function(data, req)
	{
		_waiting = false;
		try_send();
	}

	_send(req, _ok, _err);
}

function _send(req, _onOk = undefined, _onError = undefined)
{
	var url = wdlib.Config.PROJECT_URL + req.uri;

	var _err = function(err)
	{
		console.error("wdlib.net.Http::_send FAIL : ", url, err);
		if(_onError !== undefined) {
			_onError.call(undefined, err, req);
		}
	}

	try {
		fetch(url, {
			method: "post",
			headers: {
				"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
			},
			body: wdlib.net.Http.encode(req.data)
		}).then(function(response) {
			if(!response.ok) {
				throw new Error("error while send : " + url);
			}
			return response.json();
		}).then(function(data) {
			// check if error : {"error":"ERROR MESSAGE","error_code":ERROR_CODE}
			var error = undefined;
			if(data.error) {
				// some error occured
				error = new wdlib.model.Error(data);
			
				console.error("wdlib.net.Http : HTTP SEND ERROR : ", error, data);
			}

			if(req.callback) {
				req.callback.apply(req.context, [error, data, req.udata]);
			}

			if(_onOk !== undefined) {
				_onOk.call(undefined, data, req);
			}
		}).catch(function(e) {
			_err.call(undefined, e);
		});
	}
	catch(e) {
		_err.call(undefined, e);
	}
}

function _sort(a, b)
{
	if(a.k > b.k) {
		return 1;
	}
	else if(a.k < b.k) {
		return -1;
	}
	else {
		return 0;
	}
}
// ============================================================================

}, ((wdlib.net = wdlib.net || {})).Http = {});
