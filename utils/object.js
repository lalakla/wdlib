wdlib.module("wdlib/utils/object.js", [
],
function(exports) {
"use strict";

/**
 * @param Object data
 * @param string|int key
 * @param mixed def, default null
 * @return mixed
 */
exports.isset = function(data, key, def = undefined)
{
	var retval = def;
	if(data && data.hasOwnProperty(key)) {
//	if(data && (data[key] || data.hasOwnProperty(key))) {
		retval = data[key];
	}
	return retval;
}

/**
 * @param Array arr
 * @return mixed
 */
exports.randomItem = function(arr)
{
	var idx = Math.floor((Math.random() * 1000) % arr.length);
	return arr[idx];
}

/**
 * @param Array arr
 */
exports.shuffle = function(arr)
{
	arr.sort(_shuffle_sort);
}
function _shuffle_sort(a, b)
{
	return Math.round(Math.random() * 2) - 1;
}

exports.toString = function(o)
{
	return JSON.stringify(o);
}

}, ((wdlib.utils = wdlib.utils || {}).object = {}));
