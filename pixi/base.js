wdlib.module("wdlib/pixi/base.js", [
	"wdlib/events/dispatcher.js",
	"wdlib/model/disposable.js"
],
function(exports) {
"use strict";

var _loading = false;
var _to_load = [];
var _map = new Map;

var _pixi = false;

// ============================================================================
// wdlib.pixi.* interface

/**
 * @param String name
 * @param String url
 * @param Fuction callback
 */
exports.load = function(name, url, callback)
{
	if(!Array.isArray(name)) {
		wdlib.pixi.load([{name: name, url: url, callback: callback}]);
		return;
	}

	// in this case we have name = array of {name: name, url: url, callback: callback}
	var items = name;

	items.forEach(function(item) {

		var req = new Req(item.name, item.url, item.callback);

		// check if already loading
		var tl = _map.get(item.url);
		if(tl) {
			// found
			// add callback
			tl.reqs.push(req);
			return;
		}
	
		tl = new ToLoad(req.url, Object.assign({}));
		tl.reqs.push(req);
		_map.set(tl.url, tl);

		_to_load.push(tl);
	});

	try_load();
}

/*
exports.load = function(name, url, callback)
	var req = new Req(name, url, callback);

	// check if already loading
	var tl = _map.get(url);
	if(tl) {
		// found
		// add callback
		tl.reqs.push(req);
		return;
	}

	tl = new ToLoad(req.url);
	tl.reqs.push(req);
	_map.set(tl.url, tl);

	_to_load.push(tl);
	try_load();
}
*/

// ============================================================================
// test of WEB GL

var lib = (!wdlib.Config.IS_MOBILE && isWebGLSupported()) ? "pixijs" : "pixijs-legacy";
console.log("PIXI using " + lib + " version....");
wdlib.load([lib], function() {
	_pixi = true;

	// set FPS to flash defaults
	PIXI.settings.TARGET_FPMS = 0.024;

	// custom resource loader middleware
//	PIXI.Loader.shared.use(function(resource, next) {
//		console.log("PIXI CUSTOM loader middleware : ", resource);
//		next();
//	});

	try_load();
});

var supported;

function isWebGLSupported() {
	if (typeof supported === 'undefined') {
		supported = (function supported() {
			var contextOptions = {
				stencil: true,
				failIfMajorPerformanceCaveat: true /*settings.FAIL_IF_MAJOR_PERFORMANCE_CAVEAT,*/
			};
			try {
				if (!window.WebGLRenderingContext) {
					return false;
				}
				var canvas = document.createElement('canvas');
				var gl = (canvas.getContext('webgl', contextOptions)
					|| canvas.getContext('experimental-webgl', contextOptions));
				var success = !!(gl && gl.getContextAttributes().stencil);
				if (gl) {
					var loseContext = gl.getExtension('WEBGL_lose_context');
					if (loseContext) {
						loseContext.loseContext();
					}
				}
				gl = null;
				return success;
			}
			catch (e) {
				return false;
			}
		})();
	}
	return supported;
}

// ============================================================================

function try_load()
{
	if(!_pixi || _loading || !_to_load.length) {
		return;
	}

	_loading = true;

	while(_to_load.length) {
		var res = undefined;
		var tl = _to_load.shift();

		// check if already loaded
		if(res = PIXI.Loader.shared.resources[tl.url]) {
			tl.release(res, tl.url, PIXI.Loader.shared.resources);
			_map.delete(tl.url);
			continue;
		}

		PIXI.Loader.shared.add(tl.url, tl.url);
	}

	PIXI.Loader.shared.load(function(loader, resources) {
//		console.log("PIXI LOADER RESOURSES : ", resources);

		for(var res in resources) {
			var tl = _map.get(resources[res].url);
			if(!tl) {
				// some error
				continue;
			}

			tl.release(resources[res], res, resources);
			_map.delete(tl.url);
		}

		_loading = false;
		try_load();
	});
}

// ============================================================================
// internal classes

class Req extends wdlib.model.Disposable {

	constructor(name, url, callback)
	{
		super();

		this.name = name;
		this.url = url;
		this.callback = callback;
	}

	dispose()
	{
		super.dispose();

		this.callback = undefined;
		this.name = undefined;
		this.url = undefined;
	}
}

class ToLoad {

	constructor(url)
	{
		this.url = url;
		this.reqs = [];
	}

	release(data, name, resources)
	{
		for(var i=0; i<this.reqs.length; ++i) {
			this.reqs[i].callback.call(undefined, data, name, resources);
			this.reqs[i].dispose();
		}
		this.reqs = [];
	}
}

// ============================================================================

}, (wdlib.pixi = wdlib.pixi || {}));
